Title: Scalable Statistical Introgression Mapping Using Approximate Coalescent-Based Inference

Authors: Qiqige Wuyun, Kevin J. Liu

LICENSE: All source code are distributed under the terms of the GNU General Public License as published by the Free Software Foundation.
You can distribute or modify source code under the terms of the GNU General Public License either version 3 of the License
(https://www.gnu.org/licenses/gpl-3.0.txt) or any later version.
All data are distributed under the terms of the Creative Commons Attribution-ShareAlike 4.0 International license
(https://creativecommons.org/licenses/by-sa/4.0/).


The simulated and empirical datasets and the scripts used to run the experiments are located in the following folders/files:


1. simulationdata: contains inputfiles used in the simulation analysis. 

2. empiricaldata: contains inputfiles used in the empirical analysis.

3. PHiMM.jar: the main software, which is a modified version of the PhyloNet software package (see more information about packages at https://wiki.rice.edu/confluence/display/PHYLONET/HmmCommand).

Usages: java -jar -Xmx20g PHiMM.jar inputfile

